#include "..\inc\fx2.h"
#include "..\inc\fx2regs.h"
#include "..\inc\fx2sdly.h"
#include "..\inc\sw_i2c.h"

//I2C pin
#define I2C_SCL			7
#define I2C_SDA			5

#define PIN_LOW(pin)		do{ IOD &= ~(1 << pin); }while(0)//PORT_CLR(B, pin)
#define PIN_HIGH(pin)		do{ IOD |= 1 << pin; }while(0)//PORT_SET(B, pin)

#define PIN_INPUT(pin)		do{ OED &= ~(1 << pin); }while(0)//DDR_CLR(B, pin)
#define PIN_OUTPUT(pin)		do{ OED |= 1 << pin; }while(0)//DDR_SET(B, pin)

#define i2c_nop() 

#define SET_SCL()		do{ PIN_HIGH(I2C_SCL); i2c_nop(); }while(0)
#define CLR_SCL()		do{ PIN_LOW(I2C_SCL);  i2c_nop(); }while(0)

#define SET_SDA()		do{ PIN_HIGH(I2C_SDA); i2c_nop(); }while(0)
#define CLR_SDA()		do{ PIN_LOW(I2C_SDA);  i2c_nop(); }while(0)

#define GET_SDA()		(IOD & (1<<I2C_SDA))

void __delay(void)
{
	unsigned int i;

	for(i=0; i<480; i++);
}

void i2c_start(void)
{
	SET_SDA();
	SET_SCL();

	PIN_OUTPUT(I2C_SCL);
	PIN_OUTPUT(I2C_SDA);

	delay();
	CLR_SDA();
	delay();
	CLR_SCL();
}

void i2c_stop(void)
{
	//CLR_SCL();
	CLR_SDA();
	PIN_OUTPUT(I2C_SCL);
	PIN_OUTPUT(I2C_SDA);
	delay();
	SET_SCL();
	delay();
	SET_SDA();
}

unsigned char i2c_write(unsigned char _data)
{
	unsigned char mask = 0x80, ret;

	PIN_OUTPUT(I2C_SDA);
	do {
		if(mask & _data) {
			SET_SDA();
		} else {
			CLR_SDA();
		}
		delay();
		SET_SCL();
		delay();
		mask = mask >> 1;
		CLR_SCL();
	} while(mask);
	SET_SDA();
	PIN_INPUT(I2C_SDA);

	delay();
	SET_SCL();
	delay();
	ret = GET_SDA();
	CLR_SCL();
	

	return ret;
}

unsigned char i2c_read(unsigned char ack_en)
{
	unsigned char mask = 0x80, read = 0;

	do {
		SET_SCL();
		if(GET_SDA()) {
			read |= mask;
		}
		delay();
		mask = mask >> 1;
		CLR_SCL();
		delay();
	} while(mask);

	//ACK
	if(ack_en) {
		CLR_SDA();
	} else {
		SET_SDA();
	}
	PIN_OUTPUT(I2C_SDA);
	delay();
	SET_SCL();
	delay();
	CLR_SCL();
	SET_SDA();
	PIN_INPUT(I2C_SDA);

	return read;
}

