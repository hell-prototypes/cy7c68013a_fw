// ======================================================================
// Hell Logic Sniffer USB FW
// 
// Copyright 2012-2013 Hell-Prototypes
//
// http://code.google.com/p/hell-prototypes/
//
// This is free software, licensed under the terms of the GNU General
// Public License as published by the Free Software Foundation.
// ======================================================================
#define TIME_OUT			2500

#define CTRL_START()		do{I2CS |= bmSTART;}while(0)
#define CTRL_STOP()			do{I2CS |= bmSTOP;}while(0)
#define CTRL_LASTRD()		do{I2CS |= bmLASTRD;}while(0)
#define DONE_CHK()			(I2CS & bmDONE)
#define ACK_CHK()			(I2CS & bmACK)
#define STOP_CHK()			(I2CS & bmSTOP)
#define ERROR_CHK()			(I2CS & bmBERR)
#define DATA_RD()			I2DAT
#define DATA_WR(byte)		do{I2DAT = (BYTE)(byte);}while(0)
#define INT_DISABLE()		EI2C = 0;

#define AT24C64_ADDR		0xA2

void i2c_hw_init();
BOOL at24c64_write_bytes_at(BYTE dev_addr, WORD start_addr, BYTE *buffer, BYTE len);
BOOL at24c64_read_bytes_at(BYTE dev_addr, WORD start_addr, BYTE *buffer, BYTE len);
