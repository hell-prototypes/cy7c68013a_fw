#ifndef __I2C_H
#define __I2C_H

void i2c_start(void);
void i2c_stop(void);
unsigned char i2c_write(unsigned char _data);
unsigned char i2c_read(unsigned char ack_en);

void __delay(void);

#define delay	 __delay

#endif
