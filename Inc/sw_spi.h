#ifndef __SPI_H
#define __SPI_H

void spi_init(unsigned char mode);
unsigned char spi_play_one_byte(unsigned char _data);

#endif
